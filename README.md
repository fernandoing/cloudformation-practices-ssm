# Demo cloudformation

## Requirements
* [git](https://git-scm.com/)
* [httpie](https://httpie.io/)
* [make](https://www.opensourceforu.com/2012/06/gnu-make-in-detail-for-beginners/)

## Pasos para desplegar el demo

[]  git clone git@gitlab.com:fernandoing/cloudformation_practices.git

[]  copiar el archivo ./demo_Makefile a Makefile

[]  editar el Makefile para cambiar colocar los valores de las variables a
utilizar

[]  desplegar el stack con la orden `make deploy-testfernadoing`

[]  optener el id de la instancia, con la orden `make output-testfernandoing`

[]  conectarse a la instancia con la sintaxis:

    - `aws --profile <nombre_perfil_si_se tiene> --region us-west-2 ssm start-session --target <id_instancia>`

conectará con un usuario llamado ssm-user, se puede cambiar al usuario de la instancia ejecutando:

    - `sudo su - ec2-user`

[]  eliminar el stack, con la orden `make delete-testfernadoing`
